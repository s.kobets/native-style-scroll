// #1
const outer = document.createElement('div');
const inner = document.createElement('div');

outer.style.overflow = 'scroll';

document.body.appendChild(outer);
outer.appendChild(inner);
const scrollbarWidth = outer.offsetWidth - inner.offsetWidth;

console.log(scrollbarWidth);
document.body.removeChild(outer);
// exit #1

window.addEventListener(
  'scroll',
  throttle(() => {
    const scrollTop = window.scrollY;
    /* doSomething with scrollTop */
  })
);

// exampel for canceling scroll main page if current block scroll in footer
window.addEventListener('DOMContentLoaded', () => {
  const elem = document.getElementsByClassName('scroll')[0];
  const elemHeight = elem.offsetHeight;
  // для производительности, пассивный обработчик
  elem.addEventListener('touchstart', onScroll, { passive: true });

  const prevent = event => {
    event.preventDefault();
    return false;
  };

  function onScroll(event) {
    // value scroll
    const scroll =
      window.pageYOffset /
      (document.body.scrollHeight - window.innerHeight) *
      100;
    // exit
    const delta = -event.deltaY;
    if (delta < 0 && elemHeight - delta > elem.scrollHeight - elem.scrollTop) {
      elem.scrollTop = elem.scrollHeight;
      return prevent(event);
    }
    if (delta > elem.scrollTop) {
      elem.scrollTop = 0;
      return prevent(event);
    }
    return true;
  }
});

function throttle(action) {
  let isRunning = false;
  return function() {
    if (isRunning) return;
    isRunning = true;
    window.requestAnimationFrame(() => {
      action();
      isRunning = false;
    });
  };
}
