const static = require('node-static');

const file = new static.Server('./');

require('http')
  .createServer(function(req, res) {
    req
      .addListener('end', function() {
        //
        // Serve files!
        //
        file.serve(req, res);
      })
      .resume();
  })
  .listen(8080);
